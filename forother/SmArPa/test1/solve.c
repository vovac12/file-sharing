#include <stdio.h>
#include "solve.h"

void solve(int *a, int *x)
{
    int maxn;
    int minn;
    int summ;
    int i;
    maxn = a[0];
    minn = a[0];

    for(i = 1; i < *x; ++i)
    {
        if(maxn > a[i])
        {
            maxn = a[i];
        }
        if(minn < a[i])
        {
            minn = a[i];
        }
    }
    summ = minn + maxn;
    printf("Max: %d\nMin: %d\nSumm: %d\n", maxn, minn, summ);

    if (summ%2 == 0)
    {
        for(i = *x; i > 0; --i)
        {
            a[i] = a[i - 1];
        }
        a[0] = summ;
    }
    else
    {
        a[*x] = summ;
    }
	++*x;
}
