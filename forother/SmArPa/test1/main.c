#include <stdio.h>
#include "read.h"
#include "solve.h"
#include "write.h"

#define INCORRECT_INPUT 1
#define OK 0
#define MAX_LENGHT 100



int main()
{
    int x;
    int a[MAX_LENGHT];
    read(a, &x);

    write(a, x);
    solve(a, &x);
    write(a, x);
    return 0;
}
