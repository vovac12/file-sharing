#include <stdio.h>
#include "write.h"

void write(int *a, int x)
{
    int i;
	printf("Array [%d]: ", x);
    for(i = 0; i < x; i++)
    {
        printf("%d ", a[i]);
    }
	printf("\n");
}

