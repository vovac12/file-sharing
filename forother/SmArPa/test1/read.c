#include <stdio.h>
#include "read.h"

int read(int a[MAX_LENGHT], int *x)
{
    int i, c;
    printf("Please, input quantity of elements: ");
    scanf("%d", x);
    if (*x <= 0)
    {
        printf("Incorrect quantity, must be positive and not 0!\n");
        return INCORRECT_INPUT;
    }
    else
    {
        printf("Input array:");
        for(i=0; i<*x; i++)
        {
			if (scanf("%d",&c) != 1) {
				return INCORRECT_INPUT;
			}
            a[i] = c;
        }
        return OK;
    }
}
